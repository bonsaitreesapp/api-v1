FROM php:7.4-fpm

RUN apt-get update && apt-get install -y git nano wget bash

RUN apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-configure zip
RUN apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql
RUN docker-php-ext-install zip

RUN curl -sS https://getcomposer.org/installer | php -- \
--install-dir=/usr/bin --filename=composer && chmod +x /usr/bin/composer

RUN chown -R www-data:www-data /var/www

WORKDIR /var/www/api

#RUN composer install
